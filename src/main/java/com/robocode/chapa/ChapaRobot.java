package com.robocode.chapa;

import com.robocode.chapa.model.AdvancedScannedRobotEvent;
import com.robocode.chapa.track.PredictiveFireTracker;
import com.robocode.chapa.track.Tracker;
import com.robocode.chapa.track.WaveSurfingMovementTracker;
import robocode.*;
import robocode.util.Utils;

import java.awt.*;

/**
 * Created by getulio on 6/8/17.
 */
public class ChapaRobot extends AdvancedRobot{

    private Tracker fireTracker = new PredictiveFireTracker();
    private Tracker movementTracker = new WaveSurfingMovementTracker();

    @Override
    public void run() {

        //pretty body colors
        setBodyColor(new Color(255, 23, 241));
        setGunColor(new Color(255, 34, 253));
        setRadarColor(new Color(255, 23, 241));
        setBulletColor(new Color(255, 0, 0));
        setScanColor(new Color(255, 34, 253));

        //turn off radar gun sync on turn
        setAdjustGunForRobotTurn(true);
        setAdjustRadarForGunTurn(true);
        //setAdjustRadarForRobotTurn(true);


        //main loop
        do{
            turnRadarRightRadians(Double.POSITIVE_INFINITY);
        }while(true);

    }
    private int tries;

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        //basic tracking
        setTurnRadarRightRadians(Utils.normalRelativeAngle((e.getBearingRadians() + getHeadingRadians()) - getRadarHeadingRadians()) * 2);

        AdvancedScannedRobotEvent advancedScannedRobotEvent = new AdvancedScannedRobotEvent(e, this);
        movementTracker.update(advancedScannedRobotEvent);
        movementTracker.track();
        fireTracker.update(advancedScannedRobotEvent);
        fireTracker.track();
    }

    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        movementTracker.update(e, this);
    }

}
