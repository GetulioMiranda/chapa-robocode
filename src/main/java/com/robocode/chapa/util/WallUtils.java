package com.robocode.chapa.util;


import java.awt.geom.Rectangle2D;

import static com.robocode.chapa.util.PointUtils.projectPointIntoSapce;

/**
 * Created by getulio on 6/20/17.
 */
public class WallUtils {

    private static final Rectangle2D.Double battleField = new Rectangle2D.Double(18, 18, 764, 564);

    private static final double SM_FACTOR = 0.05;

    public static double wallDodge(double x, double y, double angle, int direction) {
        while (!battleField.contains(projectPointIntoSapce(x, y, angle, 140))){
            angle += direction * SM_FACTOR;
        }
        return angle;
    }

}
