package com.robocode.chapa.util;

import com.robocode.chapa.model.Part;
import robocode.AdvancedRobot;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import static robocode.util.Utils.normalRelativeAngleDegrees;

/**
 * Created by getulio on 6/12/17.
 */
public class PartsUtil {

        /*
         * method designed to turn a single part based on bearing
         */
    public static void turnPartBasedOnBearingTowardsTo(AdvancedRobot chapa, double targetBearing, Part part){
        switch (part){
            case GUN:
                chapa.setTurnGunRight(normalRelativeAngleDegrees(targetBearing + (chapa.getHeading() - chapa.getGunHeading())));
                break;
            case RADAR:
                chapa.setTurnRadarRight(normalRelativeAngleDegrees(targetBearing + (chapa.getHeading() - chapa.getRadarHeading())));
                break;
            case BODY:
                chapa.setTurnRight(targetBearing);
                break;
            default:
                chapa.out.println("Uknown body part turn.");
        }
    }

    /*
     * method designed to turn several parts based on bearing
     */
    public static void turnPartsBasedOnBearingTowardsTo(AdvancedRobot chapa, double targetBearing, Part... parts){
        for(Part part : parts){
            turnPartBasedOnBearingTowardsTo(chapa, targetBearing, part);
        }
    }

    /*
        * method designed to turn a single part based on X and Y
        */
    public static void turnPartBasedOnRelativeDegreeTowardsTo(AdvancedRobot chapa, double relativeDegree, Part part) {
        switch (part) {
            case GUN:
                chapa.setTurnGunRight(normalRelativeAngleDegrees(relativeDegree - chapa.getGunHeading()));
                break;
            case RADAR:
                chapa.setTurnRadarRight(normalRelativeAngleDegrees(relativeDegree - chapa.getRadarHeading()));
                break;
            case BODY:
                chapa.setTurnRight(relativeDegree - chapa.getHeading());
                break;
            default:
                chapa.out.println("Uknown body part turn.");
        }
    }

}
