package com.robocode.chapa.util;

import java.awt.geom.Point2D;

/**
 * Created by getulio on 6/20/17.
 */
public class PointUtils {

    public static Point2D.Double projectPointIntoSapce(double x, double y, double angle, double length) {
        return new Point2D.Double(x + Math.sin(angle) * length,y + Math.cos(angle) * length);
    }

}
