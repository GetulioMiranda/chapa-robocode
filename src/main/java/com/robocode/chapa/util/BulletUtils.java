package com.robocode.chapa.util;

/**
 * Created by getulio on 6/20/17.
 */
public class BulletUtils {

    //affects the bullet factor, change to adjust
    private static final double BULLET_SPEED_FACTOR_MULTIPLIER = 3;
    private static final double BULLET_BASE_SPEED_FACTOR = 20;

    public static double bulletSpeed(double energy){
        return (BULLET_BASE_SPEED_FACTOR - (BULLET_SPEED_FACTOR_MULTIPLIER* energy));
    }

}
