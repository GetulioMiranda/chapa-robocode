package com.robocode.chapa.model;

import java.awt.geom.Point2D;

/**
 * Created by getulio on 6/20/17.
 */
public class TrackableBullet {

    private Point2D startPoint;
    private long time;
    private double speed;
    private double angle;
    private double distance;
    private int direction;

    public TrackableBullet(Point2D startPoint, long time, double speed, double angle, double distance, int direction) {
        this.startPoint = startPoint;
        this.time = time;
        this.speed = speed;
        this.angle = angle;
        this.distance = distance;
        this.direction = direction;
    }


    public void updateDistance(long now){
        this.distance = (now - time) * speed;
    }

    public Point2D getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point2D startPoint) {
        this.startPoint = startPoint;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
