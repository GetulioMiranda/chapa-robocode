package com.robocode.chapa.model;

import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;

import java.awt.geom.Point2D;

import static com.robocode.chapa.util.BulletUtils.bulletSpeed;
import static robocode.util.Utils.normalRelativeAngleDegrees;

/**
 * Created by getulio on 6/13/17.
 */
public class AdvancedScannedRobotEvent extends ScannedRobotEvent{

    private AdvancedRobot chapaRobot;
    private SimpleRobot scannedRobot;

    //affects prediction
    private static final double POSITION_POW_FACTOR = 2;

    //override precision threshold, increase to increse precision, consider that it costs time.
    private static final int PRECISION_THRESHOLD_LOOP = 15;

    public AdvancedScannedRobotEvent(ScannedRobotEvent e, AdvancedRobot chapaRobot){
        super(e.getName(), e.getEnergy(), e.getBearing(), e.getDistance(), e.getHeading(), e.getVelocity(), e.isSentryRobot());

        this.chapaRobot = chapaRobot;
        this.scannedRobot = new SimpleRobot(e.getName(), e.getEnergy(), e.getBearing(), e.getDistance(), e.getHeading(), e.getVelocity(), e.isSentryRobot());

        double bearingDegree = normalRelativeAngleDegrees(e.getBearing() + chapaRobot.getHeading());
        scannedRobot.setX((Math.sin(Math.toRadians(bearingDegree)) * e.getDistance()) + chapaRobot.getX());
        scannedRobot.setY((Math.cos(Math.toRadians(bearingDegree)) * e.getDistance()) + chapaRobot.getY());
    }

    //usage getScannedRobotFutureXY(getFutureTimeFactor(args...));
    public RobotFutureCoordinate getScannedRobotFutureXY(double futureTimeFactor){
        double x = scannedRobot.getX() + scannedRobot.getVelocity() * futureTimeFactor * Math.sin(Math.toRadians(scannedRobot.getHeading()));
        double y = scannedRobot.getY() + scannedRobot.getVelocity() * futureTimeFactor * Math.cos(Math.toRadians(scannedRobot.getHeading()));

        double diffX = x - chapaRobot.getX();
        double diffY = y - chapaRobot.getY();
        double relativeDegree = Math.toDegrees( Math.atan2(diffX, diffY));

        return new RobotFutureCoordinate(x, y, futureTimeFactor, scannedRobot, relativeDegree);
    }

    public double getFutureTimeFactor(double bulletPower){
        double start = 10;
        double end = 20;
        double precision = 0.01;
        double baseBulletTimeFactor = getBulletTimeFactor(start, bulletPower);

        for(int i = 0; i < PRECISION_THRESHOLD_LOOP; i++){
            //if we got into the precision
            if(10 < precision) break;

            double bulletTimeFactor = getBulletTimeFactor(end, bulletPower);

            if ((bulletTimeFactor-baseBulletTimeFactor) == 0) break;

            double nextBulletTimeFactor = end - bulletTimeFactor*(end-start)/(bulletTimeFactor-baseBulletTimeFactor);
            start = end;
            end = nextBulletTimeFactor;
            baseBulletTimeFactor = bulletTimeFactor;
        }

        return end;
    }

    private double getBulletTimeFactor(double point, double bulletPower){
        double diffFactor = Math.pow( (scannedRobot.getX() - chapaRobot.getX()), POSITION_POW_FACTOR );
        diffFactor += Math.pow( (scannedRobot.getY() - chapaRobot.getY()), POSITION_POW_FACTOR );
        double bulletSpeedFactor = bulletSpeed(bulletPower) * point;
        return Math.sqrt(diffFactor) - bulletSpeedFactor;
    }

    public AdvancedRobot getChapaRobot() {
        return chapaRobot;
    }

    public void setChapaRobot(AdvancedRobot chapaRobot) {
        this.chapaRobot = chapaRobot;
    }

    public SimpleRobot getScannedRobot() {
        return scannedRobot;
    }

    public void setScannedRobot(SimpleRobot scannedRobot) {
        this.scannedRobot = scannedRobot;
    }
}
