package com.robocode.chapa.model;

/**
 * Created by getulio on 6/8/17.
 */
public enum Part {

    BODY, RADAR, GUN

}
