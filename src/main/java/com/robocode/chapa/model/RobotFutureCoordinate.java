package com.robocode.chapa.model;

import java.awt.geom.Point2D;

/**
 * Created by getulio on 6/14/17.
 */
public class RobotFutureCoordinate {

    private Point2D point;
    private double time;
    private SimpleRobot robot;
    private double relativeDregree;

    public RobotFutureCoordinate(double x, double y, double time, SimpleRobot robot, double relativeDregree) {
        this.point = new Point2D.Double(x, y);
        this.time = time;
        this.robot = robot;
        this.relativeDregree = relativeDregree;
    }

    public double getRelativeDregree() {
        return relativeDregree;
    }

    public void setRelativeDregree(double relativeDregree) {
        this.relativeDregree = relativeDregree;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Point2D getPoint() {
        return point;
    }

    public void setPoint(Point2D point) {
        this.point = point;
    }

    public SimpleRobot getRobot() {
        return robot;
    }

    public void setRobot(SimpleRobot robot) {
        this.robot = robot;
    }
}
