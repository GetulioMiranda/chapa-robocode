package com.robocode.chapa.track;

import com.robocode.chapa.model.AdvancedScannedRobotEvent;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;

/**
 * Created by getulio on 6/20/17.
 */
public interface Tracker {

    void update(AdvancedScannedRobotEvent advancedScannedRobotEvent);
    void update(HitByBulletEvent hitByBulletEvent, AdvancedRobot chapa);
    void reset();
    void track();
    boolean hasTarget();

}
