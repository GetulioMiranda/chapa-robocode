package com.robocode.chapa.track;

import com.robocode.chapa.model.AdvancedScannedRobotEvent;
import com.robocode.chapa.model.Part;
import com.robocode.chapa.model.RobotFutureCoordinate;
import com.robocode.chapa.model.SimpleRobot;
import com.robocode.chapa.util.PartsUtil;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;

/**
 * Created by getulio on 6/12/17.
 * Designed to track down targets with predictive algorithm
 */
public class PredictiveFireTracker implements Tracker {

    private AdvancedScannedRobotEvent advancedScannedRobotEvent;

    public void update(AdvancedScannedRobotEvent advancedScannedRobotEvent){
        this.advancedScannedRobotEvent = advancedScannedRobotEvent;
    }

    @Override
    public void update(HitByBulletEvent hitByBulletEvent, AdvancedRobot chapa) {

    }

    public void reset(){
        advancedScannedRobotEvent = null;
    }

    public void track(){

        //first we turn the radar to not let the target scape
        AdvancedRobot chapaRobot = advancedScannedRobotEvent.getChapaRobot();
        SimpleRobot scannedRobot = advancedScannedRobotEvent.getScannedRobot();

        //removed due issues when tracking
        //PartsUtil.turnPartBasedOnBearingTowardsTo(chapaRobot, scannedRobot.getBearing(), Part.RADAR);

        double futureTimeFactor = advancedScannedRobotEvent.getFutureTimeFactor(getBulletPower());
        RobotFutureCoordinate futureCoordinate = advancedScannedRobotEvent.getScannedRobotFutureXY(futureTimeFactor);
        PartsUtil.turnPartBasedOnRelativeDegreeTowardsTo(chapaRobot, futureCoordinate.getRelativeDregree(), Part.GUN);
        chapaRobot.setFire(getBulletPower());

        //TODO REMOVE DEBUG STUFF!!!
        //chapaRobot.out.println("target XY: " + scannedRobot.getX() + "," + scannedRobot.getY() + " || future XY: " + futureCoordinate.getPoint().getX() + "," + futureCoordinate.getPoint().getY());

    }

    private double getBulletPower(){
        return 400 / advancedScannedRobotEvent.getScannedRobot().getDistance();
    }

    public boolean hasTarget(){
        return advancedScannedRobotEvent != null;
    }

}
