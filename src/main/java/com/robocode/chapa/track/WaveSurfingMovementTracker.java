package com.robocode.chapa.track;

import com.robocode.chapa.model.AdvancedScannedRobotEvent;
import com.robocode.chapa.model.RobotFutureCoordinate;
import com.robocode.chapa.model.SimpleRobot;
import com.robocode.chapa.model.TrackableBullet;
import com.robocode.chapa.util.BulletUtils;
import com.robocode.chapa.util.PointUtils;
import com.robocode.chapa.util.WallUtils;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.util.Utils;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static com.robocode.chapa.util.BulletUtils.bulletSpeed;

/**
 * Created by getulio on 6/20/17.
 */
public class WaveSurfingMovementTracker implements Tracker{

    private static final double BULLET_MAX_THRESHOLD = 3.01;
    private static final double BULLET_MIN_THRESHOLD = 0.09;

    private static final double SCAPE_ANGLE_TRESHOLD = 8;

    private AdvancedScannedRobotEvent currentAdvancedScannedRobotEvent;
    private double pe = 100;

    private double bulletLogs[];
    private List<TrackableBullet> bullets;
    private List<Integer> directions;
    private List<Double> bearings;


    public WaveSurfingMovementTracker() {
        reset();
    }

    public void update(AdvancedScannedRobotEvent advancedScannedRobotEvent) {
        //advancedScannedRobotEvent.getChapaRobot().out.println("updating movement tracker !!! START");
        //advancedScannedRobotEvent.getChapaRobot().out.println("bullets size: " + bullets.size());
        //advancedScannedRobotEvent.getChapaRobot().out.println("directions size: " + directions.size());
        //advancedScannedRobotEvent.getChapaRobot().out.println("bearings size: " + bearings.size());
        AdvancedRobot chapaRobot = advancedScannedRobotEvent.getChapaRobot();

        this.currentAdvancedScannedRobotEvent = advancedScannedRobotEvent;


        double directionBruteValue = Math.sin(advancedScannedRobotEvent.getBearingRadians()) * chapaRobot.getVelocity();
        double bearing = advancedScannedRobotEvent.getBearingRadians() + chapaRobot.getHeadingRadians();

        directions.add(0, directionResolver(directionBruteValue));
        bearings.add(0, bearingResolver(bearing));

        //energy drop means fire!
        double energyDiff = pe - currentAdvancedScannedRobotEvent.getScannedRobot().getEnergy();
        manageBullets(energyDiff);
        pe = currentAdvancedScannedRobotEvent.getScannedRobot().getEnergy();
        update();
        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("updating movement tracker !!! FINISH");
    }

    @Override
    public void update(HitByBulletEvent hitByBulletEvent, AdvancedRobot chapa) {
        if(bullets != null && !bullets.isEmpty()){
            TrackableBullet hit = null;
            Point2D.Double chapa2DPoint = new Point2D.Double(chapa.getX(), chapa.getY());
            for (int i = 0; i < bullets.size(); i++) {
                TrackableBullet bullet = bullets.get(i);
                if(Math.abs(bullet.getDistance() - chapa2DPoint.distance(bullet.getStartPoint())) < 50
                        && Math.abs(BulletUtils.bulletSpeed(hitByBulletEvent.getBullet().getPower()) - bullet.getSpeed()) < 0.001){
                    hit = bullet;
                    break;
                }
            }

            if(hit != null){
                bullets.remove(bullets.lastIndexOf(hit));
                registerHitLog(hit, hitByBulletEvent);
            }
        }
    }

    private void registerHitLog(TrackableBullet bullet, HitByBulletEvent hitByBulletEvent) {
        int logIndex = resolveLogIndex(bullet, new RobotFutureCoordinate(hitByBulletEvent.getBullet().getX(), hitByBulletEvent.getBullet().getY(), hitByBulletEvent.getTime(), new SimpleRobot(hitByBulletEvent.getBullet().getX(), hitByBulletEvent.getBullet().getY()), 0));
        for (int i = 0; i < bulletLogs.length; i++) {
            bulletLogs[i] += 1.0 / (Math.pow(logIndex - i, 2) + 1);
        }
    }

    private void surf() {
        TrackableBullet surfableBullet = findClosestBullet();
        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("SURFING !!! " + surfableBullet);

        if(surfableBullet == null) return;

        double safeLeft = safety(surfableBullet, -1);
        double safeRight = safety(surfableBullet, 1);

        AdvancedRobot chapaRobot = currentAdvancedScannedRobotEvent.getChapaRobot();
        double angle = bearingTowards(surfableBullet.getStartPoint().getX(), surfableBullet.getStartPoint().getY(), chapaRobot.getX(), chapaRobot.getY());
        angle = WallUtils.wallDodge(chapaRobot.getX(), chapaRobot.getY(), (safeLeft < safeRight ? angle - (Math.PI/2) : angle + (Math.PI/2)), safeLeft < safeRight ? -1 : 1);

        angle = Utils.normalRelativeAngle(angle - chapaRobot.getHeadingRadians());
        if (Math.abs(angle) > (Math.PI/2)) {
            if (angle < 0) {
                chapaRobot.setTurnRightRadians(Math.PI + angle);
            } else {
                chapaRobot.setTurnLeftRadians(Math.PI - angle);
            }
                chapaRobot.setBack(100);
        } else {
            if (angle < 0) {
                chapaRobot.setTurnLeftRadians(-1*angle);
            } else {
                chapaRobot.setTurnRightRadians(angle);
            }
                chapaRobot.setAhead(100);
        }

        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("ENDED SURFING !!! " + surfableBullet);


    }

    private static final int TICKS_THRESHOLD = 500;

    public RobotFutureCoordinate estimatePositionRelativeToBullet(TrackableBullet bullet, int direction){
        AdvancedRobot chapaRobot = currentAdvancedScannedRobotEvent.getChapaRobot();
       // chapaRobot.out.println("ENTERING estimatePositionRelativeToBullet");
        int currentTicks = 0;
        boolean overlaps = false;
        SimpleRobot simpleRobot = new SimpleRobot(null, 0, 0, 0, chapaRobot.getHeadingRadians(), chapaRobot.getVelocity(), false);
        RobotFutureCoordinate robotFutureCoordinate = new RobotFutureCoordinate(
                chapaRobot.getX(), chapaRobot.getY(), chapaRobot.getTime(), simpleRobot, 0);

        double angle;
        double orientation;
        double turnThreshold;


        do{
            double currentHeading = robotFutureCoordinate.getRobot().getHeading();
            angle = WallUtils.wallDodge(
                    robotFutureCoordinate.getRobot().getX(), robotFutureCoordinate.getRobot().getY(),
                    bearingTowards(bullet.getStartPoint().getX(), bullet.getStartPoint().getY(), robotFutureCoordinate.getRobot().getX(), robotFutureCoordinate.getRobot().getY()) + (direction * (Math.PI/2)) - currentHeading,
                    direction);

            double angleCos = Math.cos(angle);

            orientation = directionResolver(angleCos);
            if(angleCos < 0)
                angle += Math.PI;

            angle = Utils.normalRelativeAngle(angle);

            turnThreshold = Math.PI/720d*(40d - 3d*Math.abs(robotFutureCoordinate.getRobot().getVelocity()));

            robotFutureCoordinate.getRobot().setHeading(Utils.normalRelativeAngle(currentHeading + limitValueBounds(-turnThreshold, angle, turnThreshold)));
            double currentSpeed = robotFutureCoordinate.getRobot().getVelocity();
            currentSpeed += (currentSpeed * orientation < 0 ? 2 * orientation : orientation);
            robotFutureCoordinate.getRobot().setVelocity(limitValueBounds(-8, currentSpeed, 8));

            Point2D.Double projectedPointIntoSapce = PointUtils.projectPointIntoSapce(robotFutureCoordinate.getRobot().getX(), robotFutureCoordinate.getRobot().getY(), robotFutureCoordinate.getRobot().getHeading(), robotFutureCoordinate.getRobot().getVelocity());
            robotFutureCoordinate.setPoint(projectedPointIntoSapce);

            currentTicks++;

            overlaps = projectedPointIntoSapce.distance(bullet.getStartPoint()) < bullet.getDistance() + (currentTicks * bullet.getSpeed()) + bullet.getSpeed();
            //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("overLAPS ***********!!! " + overlaps);
        }while(!overlaps && currentTicks < TICKS_THRESHOLD);
        //chapaRobot.out.println("LEAVING estimatePositionRelativeToBullet");
        return robotFutureCoordinate;
    }

    public double limitValueBounds(double min, double value, double max) {
        return Math.max(min, Math.min(value, max));
    }

    private double safety(TrackableBullet bullet, int direction){
        int logIndex = resolveLogIndex(bullet, estimatePositionRelativeToBullet(bullet, direction));
        return bulletLogs[logIndex];
    }

    private double bearingTowards(double x, double y, double tx, double ty) {
        return Math.atan2(tx - x, ty - y);
    }

    private int resolveLogIndex(TrackableBullet bullet, RobotFutureCoordinate robotFutureCoordinate) {
        double offsetAngle = (bearingTowards(bullet.getStartPoint().getX(), bullet.getStartPoint().getY(), robotFutureCoordinate.getRobot().getX(), robotFutureCoordinate.getRobot().getY())
                - bullet.getAngle());
        double factor = Utils.normalRelativeAngle(offsetAngle)
                / speedScapeAngle(bullet.getSpeed()) * bullet.getDirection();

        return (int)limitValueBounds(0,
                (factor * ((bulletLogs.length - 1) / 2)) + ((bulletLogs.length - 1) / 2),
                bulletLogs.length - 1);
    }

    private double speedScapeAngle(double speed) {
        return Math.asin(SCAPE_ANGLE_TRESHOLD / speed);
    }

    private TrackableBullet findClosestBullet(){
        Double closest = Double.MAX_VALUE;
        TrackableBullet result = null;
        AdvancedRobot chapaRobot = currentAdvancedScannedRobotEvent.getChapaRobot();
        Point2D.Double chapa2DPoint = new Point2D.Double(chapaRobot.getX(), chapaRobot.getY());

        for (TrackableBullet b : bullets) {
            double distance = chapa2DPoint.distance(b.getStartPoint()) - b.getDistance();
            if(distance < closest && distance > b.getSpeed()) {
                result = b;
                closest = distance;
            }
        }

        return result;
    }

    private void manageBullets(double energyDiff) {
       // currentAdvancedScannedRobotEvent.getChapaRobot().out.println("ManageBullets !!! START");
        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("******* energyDIFF: " + energyDiff);
        if(directions.size() > 2 //we are already surfing ortherwise we should wait another tick to define this
                && energyDiff < BULLET_MAX_THRESHOLD
                && energyDiff > BULLET_MIN_THRESHOLD) {
            //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FIRED!!!!! " + energyDiff);
            double speed = bulletSpeed(energyDiff);
            bullets.add(new TrackableBullet(new Point2D.Double(currentAdvancedScannedRobotEvent.getScannedRobot().getX(), currentAdvancedScannedRobotEvent.getScannedRobot().getY()),
                    currentAdvancedScannedRobotEvent.getChapaRobot().getTime() - 1, //if we detect the change that means that the fire was on the prev turn
                    speed,
                     bearings.get(2),
                     speed,
                    directions.get(2)));
        }
        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("ManageBullets !!! END");
    }
    
    private void update(){
        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("UPDATING WAVE !!! START");

        AdvancedRobot chapaRobot = currentAdvancedScannedRobotEvent.getChapaRobot();
        for (int i = 0; i < bullets.size(); i++) {
            TrackableBullet bullet = bullets.get(i);
            bullet.updateDistance(chapaRobot.getTime());
            Point2D.Double chapa2DPoint = new Point2D.Double(chapaRobot.getX(), chapaRobot.getY());
            if (bullet.getDistance() > chapa2DPoint.distance(bullet.getStartPoint()) + 50){ // if it pass :D we remove it, simple as that
                bullets.remove(i);
                i--;
            }
        }

        //currentAdvancedScannedRobotEvent.getChapaRobot().out.println("UPDATING WAVE !!! END");
    }

    private double bearingResolver(double bearing){
        return new Double(bearing + Math.PI);
    }

    private int directionResolver(double relativeSpeed){
        return relativeSpeed >= 0 ? 1 : -1;
    }

    public void reset() {
        currentAdvancedScannedRobotEvent = null;
        //previousAdvancedScannedRobotEvent = null;
        bullets = new ArrayList<TrackableBullet>();
        directions = new ArrayList<Integer>();
        bearings = new ArrayList<Double>();
        bulletLogs = new double[47];
    }

    public void track() {
        surf();
    }

    public boolean hasTarget() {
        return currentAdvancedScannedRobotEvent != null;
    }
}
